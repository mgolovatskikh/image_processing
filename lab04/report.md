## Работа 4. Использование 
автор: Головатских М.Е.

url: https://gitlab.com/mgolovatskikh/image_processing/-/tree/master/lab04

### Задание
1. Сгенерировать серое тестовое изображение из квадратов и кругов с разными уровнями яркости (0, 127 и 255) так, чтобы присутствовали все сочетания.
2. Применить первый линейный фильтр и сделать визуализацию результата  $F_1$.
3. Применить второй линейный фильтр и сделать визуализацию результата $F_2$.
4. Вычислить $R=\sqrt{F_1^2 + F_2^2}$  и сделать визуализацию R.

### Результаты

![](inital.png)
Рис. 1. Исходное тестовое изображение

![](dx.png)
Рис. 2. Визуализация результата $F_1$ применения фильтра

![](dy.png)
Рис. 3. Визуализация результата $F_2$ применения фильтра

![](gradmod.png)
Рис. 4. Визуализация модуля градиента $R$

### Текст программы

```cpp
#include <iostream>
#include <opencv2/opencv.hpp>
#include <vector>
using  namespace cv;
using namespace std;
void filtrate(Mat& src, Mat& dst, Mat& filter) {
    src.convertTo(dst, CV_32F);
    filter2D(dst, dst, -1, filter);
}

void showFiltration(Mat& src, string name) {
    Mat showing;
    src.copyTo(showing);

    showing = (showing + 255) / 2;
    showing.convertTo(showing, CV_8U);

    imshow(name, showing);
}
void writeFiltered(string path, Mat& src) {
    Mat showing;
    src.copyTo(showing);

    showing = (showing + 255) / 2;
    showing.convertTo(showing, CV_8U);

    imwrite(path, showing);
}
int main() {
    int white = 255;
    int gray = 127;
    int black = 0;
    Mat images[6];
    int COLS  = 256;
    int ROWS = 256;
    // IMAGE CREATION
    Mat result(2*ROWS, 3*COLS, CV_8UC1);
    images[0] = Mat (256,256, CV_8UC1, white );
    images[1] = Mat (256,256, CV_8UC1, black );
    images[2] = Mat (256,256, CV_8UC1, gray );
    images[3] = Mat (256,256, CV_8UC1, black );
    images[4] = Mat (256,256, CV_8UC1, gray );
    images[5] = Mat (256,256, CV_8UC1, white );
    cv::circle(images[0], cv::Point(127,127), 64, gray, -1);
    cv::circle(images[1], cv::Point(127,127), 64, white, -1);
    cv::circle(images[2], cv::Point(127,127), 64, black, -1);
    cv::circle(images[3], cv::Point(127,127), 64, white, -1);
    cv::circle(images[4], cv::Point(127,127), 64, black, -1);
    cv::circle(images[5], cv::Point(127,127), 64, gray, -1);
    for (int i_cols = 0 ; i_cols < 3; i_cols++ ){
        for (int i_rows = 0; i_rows < 2; i_rows++){
            images[i_cols + i_rows].copyTo(result(Rect(i_cols*COLS, i_rows*ROWS, COLS, ROWS)));
        }
    }
    images[5].copyTo(result(Rect(2*COLS, 1*ROWS, COLS, ROWS)));
    imshow( "source", result);
    // FILTERING
    Mat_<double > kernel(2,2), kernelT(2,2);;
    kernel << 1, 0, 0, -1;
    kernelT << 0, 1, -1, 0;
    Mat filtered, filteredT, filterComb,filtered_float, filteredT_float;
    filtrate(result, filtered, kernel);
    showFiltration(filtered, "filtered");
    writeFiltered("../lab04/dx.png", filtered);
    filtrate(result, filteredT, kernelT);
    showFiltration(filteredT, "filteredT");
    writeFiltered("../lab04/dy.png", filteredT);
    pow(filtered, 2, filtered);
    pow(filteredT, 2, filteredT);
    sqrt((filtered + filteredT), filterComb);
    showFiltration(filterComb, "filterComb");
    writeFiltered("../lab04/gradmod.png", filterComb);
    imwrite("../lab04/inital.png", result);


    waitKey(0);
    return 0;
}
```
