# 1. Гамма-коррекция серой градиентной заливки
автор: Головатских М.Е.\
url: https://gitlab.com/mgolovatskikh 
### Задание
1. Часть 1
    1. Нарисовать прямоугольник размером 768х60 пикселя с плавным изменение пикселей от черного к белому, одна градация серого занимает 3 пикселя по горизонтали.
    2. Под ним  изображение этого градиента после гамма-коррекции с коэффициентом из интервала 2.2-2.4
2. Часть 2
    1. Нарисовать прямоугольник размером 768х60 пикселя со ступенчатым изменение пикселей от черного к белому (от 5 с шагом 10), одна градация серого занимает 30 пикселя по горизонтали.
    2. изображение этого градиента после гамма-коррекции с коэффициентом из интервала 2.2-2.4

### Результаты
![Result 1](./part1.jpeg)\
Рис. 1.  Гамма-коррекция градиента с шагом 3 пикселя
![Result 1](./part2.jpeg)\
Рис. 1.  Гамма-коррекция градиента с шагом 10 пикселей

### Код программы 
* Часть 1
    ```c++
    #include <iostream>
    #include <opencv2/opencv.hpp>
    using  namespace cv;
    using namespace std;
    int main() {
        Mat M(60, 256*3, CV_8UC3 ,Scalar(0));
        for (int i_cols = 0; i_cols < M.cols; i_cols++)
            M.col(i_cols).setTo(i_cols/3);
        Mat M_float, M_float_pow, M_new;
        M.convertTo(M_float, CV_32FC1, 1.0/255.0);
        cv::pow(M_float, 2.2, M_float_pow);
        M_float_pow.convertTo(M_new, CV_8UC3, 255);
        Mat image(120,256*3, CV_8UC3);
        M.copyTo(image(Rect(0,0, M.cols,M.rows)));
        M_new.copyTo( image(Rect(0,60,M_new.cols,M_new.rows)));
        imshow("Result", image);
        imwrite("../lab01/part1.jpeg", image);
        waitKey(0);
    }
    ```
* Часть 2
    ```c++
    #include <iostream>
    #include <opencv2/opencv.hpp>
    using  namespace cv;
    using namespace std;
    int main() {
        int height = 60;
        int width = 768;
        Mat M(height, width, CV_8UC3);
        int color = 5;
        for (int i_cols = 0; i_cols < M.cols; i_cols++)
            M.col(i_cols).setTo(5 + i_cols/30*10);
        Mat M_float, M_float_pow, M_new;
        M.convertTo(M_float, CV_32FC1, 1.0/255.0);
        cv::pow(M_float, 2.2, M_float_pow);
        M_float_pow.convertTo(M_new, CV_8UC3, 255);
        Mat image(2*height,width, CV_8UC3);
        M.copyTo(image(Rect(0,0, M.cols,M.rows)));
        M_new.copyTo( image(Rect(0,height,M_new.cols,M_new.rows)));
        imwrite("../lab01/part2.jpeg", image);
        imshow("Result", image);
        waitKey(0);
        return 0;
    }
    ```   