## Работа 3. Обработка изображения в градациях серого
автор: Головатских М.Е.
дата: @time_stemp@

Url: https://gitlab.com/mgolovatskikh/image_processing/-/tree/master/lab03

### Задание
1. Подобрать и зачитать небольшое изображение $S$ в градациях серого.
2. Построить и нарисовать гистограмму $H_s$ распределения яркости пикселей исходного изображения.
3. Сгенерировать табличную функцию преобразования яркости. Построить график $V$ табличной функции преобразования яркости.
4. Применить табличную функцию преобразования яркости к исходному изображению и получить $L$, нарисовать гистограмму $H_L$ преобразованного изображения.
5. Применить CLAHE с тремя разными наборами параметров (визуализировать обработанные изображения $C_i$ и их гистограммы $H_{C_{i}}$).
6. Реализовать глобальный метод бинаризации (подобрать порог по гистограмме, применить пороговую бинаризацию). Визуализировать на одном изображении исходное $S$ и бинаризованное $B_G$ изображения.
7. Реализовать метод локальной бинаризации. Визуализировать на одном изображении исходное $S$ и бинаризованное $B_L$ изображения.
8. Улучшить одну из бинаризаций путем применения морфологических фильтров. Визуализировать на одном изображении бинарное изображение до и после фильтрации $M$.
9. Сделать визуализацию $K$ бинарной маски после морфологических фильтров поверх исходного изображения (могут помочь подсветка цветом и альфа-блендинг).


### Результаты

![](lab03.src.png)
Рис. 1. Исходное полутоновое изображение $S$

![](lab03.hist.src.png)
Рис. 2. Гистограмма $H_s$ исходного полутонового изображения $S$

![](lab03.lut.png)
Рис. 3. Визуализация функции преобразования $V$

![](lab03.lut.src.png)
Рис. 4.1. Таблично пребразованное изображение $L$

![](lab03.hist.lut.src.png)
Рис. 4.2. Гистограмма $H_L$ таблично-пребразованного изображения $L$

![](lab03.clahe.1.png)
Рис. 5.1. Преобразование $C_1$ CLAHE с параметрами claheClip = 14, basis = (18, 14)

![](lab03.hist.clahe.1.png)
Рис. 5.2. Гистограмма $H_{C_{1}}$

![](lab03.clahe.2.png)
Рис. 5.3. Преобразование $C_2$ CLAHE с параметрами claheClip = 5, basis = (5, 5)

![](lab03.hist.clahe.2.png)
Рис. 5.4. Гистограмма $H_{C_{2}}$

![](lab03.clahe.3.png)
Рис. 5.5. Преобразование $C_3$ CLAHE с параметрами claheClip = 50, basis = (25, 25)

![](lab03.hist.clahe.3.png)
Рис. 5.6. Гистограмма $H_{C_{3}}$

![](lab03.bin.global.png)
Рис. 6. Изображение $S$ до и $B_G$ после глобальной бинаризации

![](lab03.bin.local.png)
Рис. 7. Изображение $S$ до и $B_L$ после локальной бинаризации

![](lab03.morph.png)
Рис. 8. До и после морфологической фильтрации $M$

![](lab03.mask.png)
Рис. 9. Визуализация маски $K$

### Текст программы

```cpp
#include <iostream>
#include <opencv2/opencv.hpp>
#include <vector>
using  namespace cv;
using namespace std;
vector<int> createLUT() {
    vector<int> LUT(256);
    for(size_t i = 0; i < LUT.size(); i++)
    {
        if(i < 80)
        {
            LUT[i] = 240;
        }
        else if(i >= 80 && i <= 160)
        {
            LUT[i] = 40;
        }
        else if(i > 160)
        {
            LUT[i] = 120;
        }
    }
    return LUT;
}
void drawLUT(vector<int>& lut)
{
    Mat lutImage(Size(257, 257), CV_8U, Scalar(0));
    for(size_t pix = 0; pix < 256; pix++)
    {
        int value = lut[pix];
        lutImage.at<unsigned char>(value, pix) = 255;
    }
    flip(lutImage, lutImage, 0);
    imwrite("../lab03/lab03.lut.png", lutImage);
}

Mat applyLUT(Mat & src){
    vector<int> lut = createLUT();
    Mat dst = src.clone();
    LUT(src, lut, dst);
    return dst;
}
Mat applyCLAHE(Ptr<CLAHE> clahe, Mat& src){
    Mat dst;
    clahe->apply(src, dst);
    return dst;
}
Mat createHistCol(int rows, int count){
    Mat res(rows,1, CV_8UC1, Scalar(255));
    for (int i = 0; i < count; i++)
        res.at<uchar>(rows - i,1) = 0;
    return res;
}
Mat drawHist(Mat &src, int COLS, int ROWS){
    vector<int> histData(255, 0);
    int maxElement = 0;
    for (int i_row = 0; i_row < ROWS; i_row++ )
        for (int i_col = 0; i_col < COLS; i_col++ ){
            int color = src.at<uchar>(i_row, i_col);
            maxElement = maxElement < ++histData[color] ? histData[color] : maxElement;
        }
    Mat histImage = Mat(256, 256, CV_8U);
    for (int i = 0; i < 256; i++){
        int color = round(histData[i]* 255./maxElement);
        Mat curCol = createHistCol(256, color);
        curCol.copyTo(histImage(Rect(i,0,1,256)));
    }
    return histImage;
}
int main() {
    Mat original_image = imread("../pictures/fruits.png");
    int const ROWS = original_image.rows;
    int const COLS = original_image.cols;
    Mat gray_image, changedImage;
    cvtColor(original_image, gray_image, COLOR_RGB2GRAY);
    Mat histImage = drawHist(gray_image, COLS, ROWS);

    vector<int> lut = createLUT();
    drawLUT(lut);
    changedImage = applyLUT(gray_image);
    changedImage.convertTo(changedImage, CV_8UC1);
    Mat histChangedImage = drawHist(changedImage, COLS, ROWS);

    Ptr<CLAHE> clahe = createCLAHE();
    Mat claheImg1, claheImg2, claheImg3;
    claheImg1 = applyCLAHE(clahe, gray_image);
    Mat clahe1Hist = drawHist(claheImg1, COLS, ROWS);
    clahe->setClipLimit(5);
    clahe->setTilesGridSize(Size(5,5));
    claheImg2 = applyCLAHE(clahe, gray_image);
    Mat clahe2Hist = drawHist(claheImg2, COLS, ROWS);

    clahe->setClipLimit(50);
    clahe->setTilesGridSize(Size(25,25));
    claheImg3 = applyCLAHE(clahe, gray_image);
    Mat clahe3Hist = drawHist(claheImg3, COLS, ROWS);

    Mat globalBin, globalBinCompare;
    threshold(gray_image, globalBin,0, 255, THRESH_BINARY | THRESH_OTSU);
    globalBinCompare.push_back(globalBin);
    globalBinCompare.push_back(gray_image);

    Mat localBin, localBinCompare;
    adaptiveThreshold(gray_image, localBin, 255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY, 3, 2);
    localBinCompare.push_back(localBin);
    localBinCompare.push_back(gray_image);


    Mat morph, morphCompare;
    Mat k = Mat::ones(3, 3, CV_32F);
    morphologyEx(globalBin, morph, MORPH_CLOSE, k);
    morphCompare.push_back(morph);
    morphCompare.push_back(globalBin);

    Mat binaryMask;
    double alpha = 0.5; double beta = 1. - alpha;
    addWeighted(globalBin, alpha, gray_image, beta, 0. , binaryMask);
    imwrite("../lab03/lab03.mask.png", binaryMask);
    imwrite("../lab03/lab03.morph.png", morphCompare);
    imwrite("../lab03/lab03.bin.local.png", localBinCompare);
    imwrite("../lab03/lab03.bin.global.png", globalBinCompare);
    imwrite("../lab03/lab03.hist.clahe.1.png", clahe1Hist);
    imwrite("../lab03/lab03.hist.clahe.2.png", clahe2Hist);
    imwrite("../lab03/lab03.hist.clahe.3.png", clahe3Hist);
    imwrite("../lab03/lab03.clahe.1.png", claheImg1);
    imwrite("../lab03/lab03.clahe.2.png", claheImg2);
    imwrite("../lab03/lab03.clahe.3.png", claheImg3);
    imwrite("../lab03/lab03.hist.lut.src.png", histChangedImage);
    imwrite("../lab03/lab03.lut.src.png", changedImage);
    imwrite("../lab03/lab03.src.png", gray_image);
    imwrite("../lab03/lab03.hist.src.png", histImage);
    return 0;
}
```
