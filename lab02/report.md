## Работа 2. Визуализация искажений jpeg-сжатия
автор: Головатских М.Е.\
url: https://gitlab.com/mgolovatskikh/image_processing/-/tree/master/lab02

### Задание
Для исходного изображения (сохраненного без потерь) создать jpeg версии с двумя уровнями качества (например, 95 и 65). Вычислить и визуализировать на одной “мозаике” исходное изображение, результаты сжатия, поканальные и яркостные различия.

### Результаты

![](../pictures/cross.png)
Рис 1. Исходное изображение

![](result.png)
Рис 2. Результаты работы программы 

### Текст программы
* Сжатие изображений
    ```cpp
    #include <iostream>
    #include <opencv2/opencv.hpp>
    #include <vector>
    using  namespace cv;
    using namespace std;
    int main() {
        Mat original_image = imread("../pictures/cross.png");
        vector<int> save_params;
        save_params.push_back(IMWRITE_JPEG_QUALITY);
        save_params.push_back(65);
        // default quality is 95
        imwrite("../pictures/cross_95_quality.jpeg", original_image);
        imwrite("../pictures/cross_65_quality.jpeg", original_image, save_params);
        return 0;
    }
    ```
* Поканальные и яркостные различия
    ```c++
    #include <iostream>
    #include <opencv2/opencv.hpp>
    #include <vector>
    using  namespace cv;
    using namespace std;
    int main() {
        Mat original_image = imread("../pictures/cross.png");
        Mat original_gs = imread("../pictures/fruits.png", COLOR_BGR2GRAY);
        const int COLS = original_image.cols;
        const int ROWS = original_image.rows;
        const int COLOR_BUST = 20; // better visibility
        Mat quality_65_image = imread("../pictures/cross_65_quality.jpeg");
        Mat quality_95_image = imread("../pictures/cross_95_quality.jpeg");
        Mat result(2*original_image.rows, 4*original_image.cols, CV_8UC3);
    
        vector<Mat> images[3];
        Mat q65_chans[3], q95_chans[3], orig_chans[3];
        split(quality_65_image, q65_chans);
        split(quality_95_image, q95_chans);
        split(original_image, orig_chans);
        for (int i = 0; i < 3 ; i++){
            Mat q65_cur_chans[3];
            Mat q65_cur_image(COLS, ROWS, CV_8UC3);
            Mat q95_cur_chans[3];
            Mat q95_cur_image(COLS, ROWS, CV_8UC3);
            for (int i_chanel = 0; i_chanel < 3; i_chanel++) {
                q65_cur_chans[i_chanel] = i_chanel == i ? abs(q65_chans[i_chanel] - orig_chans[i_chanel]) : Mat::zeros(ROWS, COLS, CV_8UC1);
                q95_cur_chans[i_chanel] = i_chanel == i ? abs(q95_chans[i_chanel] - orig_chans[i_chanel]): Mat::zeros(ROWS, COLS, CV_8UC1);
            }
            merge(q65_cur_chans,3, q65_cur_image);
            q65_cur_image = (q65_cur_image) * COLOR_BUST;
            q65_cur_image.copyTo(result(Rect( i*COLS, 0, COLS, ROWS )));
            merge(q95_cur_chans,3, q95_cur_image);
            q95_cur_image *= COLOR_BUST;
            q95_cur_image.copyTo(result(Rect( i*COLS, ROWS, COLS, ROWS )));
        }
        Mat hsv_original, hsv_q65, hsv_q95;
        cvtColor(original_image, hsv_original, COLOR_BGR2HSV);
        cvtColor(quality_65_image, hsv_q65, COLOR_BGR2HSV);
        cvtColor(quality_95_image, hsv_q95, COLOR_BGR2HSV);
        Mat q65_diff_hsv, q95_diff_hsv;
        absdiff(hsv_original, hsv_q65, q65_diff_hsv);
        absdiff(hsv_original, hsv_q95, q95_diff_hsv);
        Mat hsv_planes[3];
        vector<Mat> images_hsv;
        images_hsv.push_back(q65_diff_hsv * COLOR_BUST);
        images_hsv.push_back(q95_diff_hsv * COLOR_BUST);
        for (int i = 0; i < 2; i++){
            Mat cur_image = images_hsv[i];
            cv::split(cur_image, hsv_planes);
            hsv_planes[0] = hsv_planes[1] =  Mat::zeros(ROWS, COLS, CV_8UC1);
            cv::merge(hsv_planes, 3, cur_image);
            cvtColor(cur_image, cur_image, COLOR_HSV2BGR);
            cur_image.copyTo(result(Rect(3*COLS, i*ROWS, COLS, ROWS)));
        }
        imshow("res", result);
        waitKey(0);
        imwrite("../lab02/result.png", result);
    }
    ```